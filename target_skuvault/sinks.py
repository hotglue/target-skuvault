"""Skuvault target sink class, which handles writing streams."""

import time
import logging
import requests
from singer_sdk.sinks import BatchSink


class SkuvaultSink(BatchSink):
    """Skuvault target sink class."""

    max_size = 100
    base_url = "https://app.skuvault.com/api"
    create_pos = []
    update_pos = []

    def process_record(self, record: dict, context: dict) -> None:

        if self.stream_name == "updatePOs":
            # po = self.get_purchase_order(record["PoNumber"])
            # if "PoId" in po:
            #     record["PurchaseOrderId"] = int(po["PoId"])
            self.update_pos.append(record)
        else:
            record["TenantToken"] = self.config.get("tenant_token")
            record["UserToken"] = self.config.get("user_token")
            self.create_pos.append(record)

    @property
    def get_headers(self):
        headers = {}
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"
        return headers

    def get_purchase_order(self, po_number):
        url = f"{self.base_url}/purchaseorders/getPOs"
        payload = {}
        payload["PONumbers"] = [po_number]
        payload["TenantToken"] = self.config.get("tenant_token")
        payload["UserToken"] = self.config.get("user_token")
        res = requests.post(url, headers=self.get_headers, json=payload)
        res = res.json()["PurchaseOrders"]
        if len(res) > 0:
            res = res[0]
        else:
            res = []
        time.sleep(6)
        return res

    def process_batch(self, context: dict) -> None:

        if self.stream_name == "updatePOs":
            url = f"{self.base_url}/purchaseorders/updatePOs"
            po_record = {}
            po_record["TenantToken"] = self.config.get("tenant_token")
            po_record["UserToken"] = self.config.get("user_token")
            po_record["POs"] = self.update_pos
            res = requests.post(url, headers=self.get_headers, json=po_record)
            logging.info(f"API response {res.status_code}: {res.text}")
            time.sleep(12)
        else:
            url = f"{self.base_url}/purchaseorders/createPO"
            for payload in self.create_pos:
                res = requests.post(url, headers=self.get_headers, json=payload)
                logging.info(f"API response {res.status_code}: {res.text}")
                time.sleep(6)
