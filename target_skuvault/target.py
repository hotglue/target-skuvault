"""Skuvault target class."""

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_skuvault.sinks import SkuvaultSink


class TargetSkuvault(Target):
    """Sample target for Skuvault."""

    name = "target-skuvault"
    config_jsonschema = th.PropertiesList(
        th.Property("tenant_token", th.StringType, required=True),
        th.Property("user_token", th.StringType, required=True),
    ).to_dict()
    default_sink_class = SkuvaultSink


if __name__ == "__main__":
    TargetSkuvault.cli()
